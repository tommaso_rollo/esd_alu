#ifndef alu_reg_HPP
#define alu_reg_HPP

#include "REG.hpp"		// REGISTERs for the inputs
#include "alu.hpp"		// ALU
#include "REG32.hpp"		// REGISTER for the output


using namespace std;


SC_MODULE(alu_reg) {
  
  // ALU_REG entity
  sc_in<sc_uint<16> > alu_reg_in1, alu_reg_in2;
  sc_in<bool>  alu_reg_op1, alu_reg_op2, alu_reg_LR, alu_reg_line;
  sc_in<bool>  alu_reg_clk, alu_reg_load1, alu_reg_load2, alu_reg_load3;
  sc_in<sc_uint<4> >  alu_reg_sel;
  sc_out<sc_uint<32> > alu_reg_out;


  // Internal signals
  sc_signal<sc_uint<16> > reg16_temp1, reg16_temp2;
  sc_signal<sc_uint<32> > reg32_temp;
  
  // Components instantiation
  REG reg16_1;
  REG reg16_2;
  alu alu_1;
  REG32 reg32_1;

  alu_reg(sc_module_name name);  
};

#endif

