// 16bit 3 port multpilexer

#ifndef mux2_HPP
#define mux2_HPP

#define NBIT 16

#include <math.h> 

SC_MODULE(mux2){

	sc_in<sc_uint<NBIT> > mux2_in1,mux2_in2;
	sc_in<bool> mux2_sel;
	sc_out<sc_uint<NBIT> > mux2_output;
	

	SC_CTOR(mux2){
		SC_THREAD(mux2_operate);
		sensitive << mux2_in1 << mux2_in2;}

	void mux2_operate();
};

#endif
