// 16bit 3 port multpilexer

#ifndef MUX4_HPP
#define MUX4_HPP

#define NBIT 16
#define SELBIT 2
#include <math.h> 

SC_MODULE(MUX4){

	sc_in<sc_uint<NBIT> > MUX4_in1,MUX4_in2,MUX4_in3,MUX4_in4;
	sc_in<sc_uint<SELBIT> > MUX4_sel;
	sc_out<sc_uint<NBIT> > MUX4_output;
	

	SC_CTOR(MUX4){
		SC_THREAD(MUX4_operate);
		sensitive << MUX4_in1 << MUX4_in2<< MUX4_in3<< MUX4_in4;}

	void MUX4_operate();
};

#endif
