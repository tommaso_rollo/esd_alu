//////////////////////////////////////
// component for M. Simonetti's ALU://
// 16bit input path, 32bit output   //
//////////////////////////////////////

#ifndef PATH_HPP
#define PATH_HPP

#include "alu2.hpp"
#include "REG.hpp"
#include "REG32.hpp"

#define PATHBIT 16
#define PATHSEL 4

using namespace std;

SC_MODULE(path){

	//entity
	sc_in<sc_uint<PATHBIT> > in_path1, in_path2;	// registers input
	sc_in<sc_uint<PATHSEL> > path_operation;	// alu select
	sc_in<bool>	path_shift, path_add1, path_add2, load_in1, load_in2, load_out;	// shifter, add/incr, registers controls
//	sc_in<bool>	path_clk;				// clock

	sc_out<<2*PATHBIT> > out_path;
	
	// internals
	sc_signal<sc_uint<PATHBIT> > out_reg1, outreg2;	// alu inputs
	sc_signal<sc_uint<2*PATHBIT> > out_alu;		// alu outputs
	sc_clock path_clk ("path_clk",10,SC_NS,0.5);
	
	//components
	alu2 alu;
	REG reg_in1, reg_in2;
	REG32 reg_out;	// 32bit reg

	path(sc_module_name name);  
};

#endif
