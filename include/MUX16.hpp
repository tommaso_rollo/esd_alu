// 32bit 16 port multpilexer

#ifndef MUX16_HPP
#define MUX16_HPP

#define MNBIT 32
#define MUXSELBIT 4
#include <math.h> 

SC_MODULE(MUX16){

	sc_in<sc_uint<MNBIT> > MUX16_in1,MUX16_in2,MUX16_in3,MUX16_in4,MUX16_in5,MUX16_in6,MUX16_in7,MUX16_in8,MUX16_in9,MUX16_in10,MUX16_in11,MUX16_in12,MUX16_in13,MUX16_in14,MUX16_in15,MUX16_in16;
	sc_in<sc_uint<MUXSELBIT> > MUX16_sel;
	sc_out<sc_uint<MNBIT> > MUX16_output;
	

	SC_CTOR(MUX16){
		SC_THREAD(MUX16_operate);
		sensitive << MUX16_in1 << MUX16_in2<< MUX16_in3<< MUX16_in4<< MUX16_in5<< MUX16_in6<< MUX16_in7<< MUX16_in8 << MUX16_in9 << MUX16_in10<< MUX16_in11<< MUX16_in12<< MUX16_in13<< MUX16_in14<< MUX16_in15<< MUX16_in16;}

	void MUX16_operate();
};

#endif
