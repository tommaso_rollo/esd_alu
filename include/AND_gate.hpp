// 16bit AND logic port

#ifndef AND_gate_HPP
#define AND_gate_HPP

#define ANDBIT 16

SC_MODULE(AND_gate){
	
	sc_in<sc_uint<ANDBIT> > AND_input1;
	sc_in<sc_uint<ANDBIT> > AND_input2;
	sc_out<sc_uint<2*ANDBIT> > AND_output;
	

	SC_CTOR(AND_gate){
		SC_THREAD(andgate_operate);
		sensitive << AND_input1 << AND_input2;}

	void andgate_operate();
};

#endif
