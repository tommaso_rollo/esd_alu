// 16bit Full Adder

#ifndef FA_HPP
#define FA_HPP

#define ANBIT 16

SC_MODULE(FA){
	
	sc_in<sc_uint<ANBIT> > FA_input1;
	sc_in<sc_uint<ANBIT> > FA_input2;

	sc_out<sc_uint<2*ANBIT> > FA_output;
	//sc_out<sc_logic> FA_over;

	SC_CTOR(FA){
		SC_THREAD(FA_operate);
		sensitive << FA_input1 << FA_input2;}

	void FA_operate();
};

#endif
