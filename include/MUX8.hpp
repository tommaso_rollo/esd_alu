// 16bit 8 port multpilexer

#ifndef MUX8_HPP
#define MUX8_HPP

#define NBIT 16
#define SELBIT 3
#include <math.h> 

SC_MODULE(MUX8){

	sc_in<sc_uint<NBIT> > MUX8_in1,MUX8_in2,MUX8_in3,MUX8_in4,MUX8_in5,MUX8_in6,MUX8_in7,MUX8_in8;
	sc_in<sc_uint<SELBIT> > MUX8_sel;
	sc_out<sc_uint<NBIT> > MUX8_output;
	

	SC_CTOR(MUX8){
		SC_THREAD(MUX8_operate);
		sensitive << MUX8_in1 << MUX8_in2<< MUX8_in3<< MUX8_in4<< MUX8_in5<< MUX8_in6<< MUX8_in7<< MUX8_in8;}

	void MUX8_operate();
};

#endif
