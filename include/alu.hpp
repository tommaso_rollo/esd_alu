#ifndef ALU_HPP
#define ALU_HPP

#define ALUNBIT 16
#define ALUSELBIT 4

#include "add_incr.hpp"		// adder and incr
#include "Multi.hpp"		// multiplier

#include "SR.hpp"		// l/r shifter
#include "Comp.hpp"		// comparator

#include "NOT_gate.hpp"		// logics
#include "AND_gate.hpp"
#include "OR_gate.hpp"
#include "XOR_gate.hpp"
#include "nand_gate.hpp"
#include "nor_gate.hpp"
#include "xnor_gate.hpp"

#include "MUX16.hpp"		// multiplexers
#include "mux2.hpp"

using namespace std;

SC_MODULE(alu) {
	// entity
	sc_in <sc_uint<ALUNBIT> > alu_in1, alu_in2;
	sc_in <sc_uint<ALUSELBIT> > alu_sel;	// select operation
	sc_in <bool> alu_line, alu_LR;	// select which line for 1-input gates & L/R shift
	sc_in <bool> alu_op1, alu_op2;	// adder function select

	sc_out <sc_uint<2*ALUNBIT> > alu_out;
//	sc_out <sc_logic> alu_of;	// alu (adder) overflow

	// internals
	sc_signal<sc_uint<2*ALUNBIT> > outnot, outshf, outand, outor, outxor, outnor, outnand, outxnor, outcomp, outadd, outmult, zeros;
	sc_signal<sc_uint<ALUNBIT> > mux2_not, mux2_shf;

	//components
	add_incr 	adder1;
	Multi 		multi1;
	SR 		sr1;
	Comp 		comp1;
	NOT_gate 	not1;
	AND_gate 	and1;
	OR_gate 	or1;
	XOR_gate 	xor1;
	nand_gate 	nand1;
	nor_gate 	nor1;
	xnor_gate 	xnor1;

	MUX16 		muxout;
	mux2		mux_sel1,mux_sel2;

	alu(sc_module_name name);  
};

#endif
