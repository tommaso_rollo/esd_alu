// 16bit OR logic port

#ifndef OR_gate_HPP
#define OR_gate_HPP

#define ORNBIT 16

SC_MODULE(OR_gate){
	
	sc_in<sc_uint<ORNBIT> > OR_input1;
	sc_in<sc_uint<ORNBIT> > OR_input2;
	sc_out<sc_uint<2*ORNBIT> > OR_output;
	

	SC_CTOR(OR_gate){
		SC_THREAD(orgate_operate);
		sensitive << OR_input1 << OR_input2;}

	void orgate_operate();
};

#endif
