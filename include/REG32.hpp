// register

#ifndef REG32_HH
#define REG32_HH

#define REGBIT2 32

SC_MODULE(REG32){

	sc_in<sc_uint<REGBIT2> > reg32_in;	// input
    	sc_in<bool> load;			// update abilitation
    	sc_in<bool> clk;			// clock

	sc_out<sc_uint<REGBIT2> > reg32_out;

	SC_CTOR(REG32){
		SC_METHOD(reg32_operate);
		sensitive << reg32_in << load << clk.pos();}	// positive clock sensitive

	void reg32_operate();
};

#endif
