// register

#ifndef REG_HH
#define REG_HH

#define REGBIT 16

SC_MODULE(REG){

	sc_in<sc_uint<REGBIT> > reg_in;		// input
    	sc_in<bool> load;			// update abilitation
    	sc_in<bool> clk;			// clock

	sc_out<sc_uint<REGBIT> > reg_out;

	SC_CTOR(REG){
		SC_METHOD(reg_operate);
		sensitive << reg_in << load << clk.pos();}	// positive clock sensitive

	void reg_operate();
};

#endif
