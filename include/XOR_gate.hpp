// 16bit XOR logic port

#ifndef XOR_gate_HPP
#define XOR_gate_HPP

#define XORNBIT 16

SC_MODULE(XOR_gate){
	
	sc_in<sc_uint<XORNBIT> > XOR_input1;
	sc_in<sc_uint<XORNBIT> > XOR_input2;
	sc_out<sc_uint<2*XORNBIT> > XOR_output;
	

	SC_CTOR(XOR_gate){
		SC_THREAD(xorgate_operate);
		sensitive << XOR_input1 << XOR_input2;}

	void xorgate_operate();
};

#endif
