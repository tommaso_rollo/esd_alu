// 16bit 2 input Multiplexer
#include <systemc.h>
#include "mux2.hpp"

using namespace std;

void mux2::mux2_operate(){
		
	sc_uint<NBIT> m1,m2;
	bool sel;
	sc_uint<NBIT> output;

	while(true){
		wait();
		sel = mux2_sel->read();

		m1 = mux2_in1->read();
		m2 = mux2_in2->read();
				
		switch(sel){
			case 0 : output = m1;	break;
			case 1 : output = m2;	break;
		}

	mux2_output->write(output);
	}
}
