// from M. Simonetti repo
#include <systemc.h>
#include "add_incr.hpp"

using namespace std;

SC_HAS_PROCESS(add_incr);

add_incr::add_incr(sc_module_name name): 
	sc_module(name), 
        add1("add1"), mux2_1("mux2_1"), mux2_2("mux2_2") {

  // Port Map
  number_one = 1;
  mux2_1.mux2_in1(add_incr_in1);
  mux2_1.mux2_in2(number_one);
  mux2_1.mux2_sel(add_incr_op1);
  mux2_1.mux2_output(mux1_out);

  mux2_2.mux2_in1(number_one);
  mux2_2.mux2_in2(add_incr_in2);
  mux2_2.mux2_sel(add_incr_op2);
  mux2_2.mux2_output(mux2_out);

  add1.FA_input1(mux1_out);
  add1.FA_input2(mux2_out);
  add1.FA_output(add_incr_out);
//  add1.FA_over(add_incr_of);
  
  
}
