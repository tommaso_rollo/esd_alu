// 16bit Full Adder
#include <systemc.h>
#include <math.h>
#include "FA.hpp"

using namespace std;

void FA::FA_operate(){
	
	sc_uint<2*ANBIT> temp_out;

	while(true){
		wait();
		
//		if(FA_input1->read() + FA_input2->read() > pow(2,ANBIT)-1)
//			FA_over->write(SC_LOGIC_1);
//		else
//			FA_over->write(SC_LOGIC_0);

		temp_out = FA_input1->read() + FA_input2->read();

		FA_output->write(temp_out);	
	}
}
