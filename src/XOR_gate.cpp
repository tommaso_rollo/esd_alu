// 16bit XOR logic gate
#include <systemc.h>
#include "XOR_gate.hpp"

using namespace std;

void XOR_gate::xorgate_operate(){
	
	sc_lv<XORNBIT> temp_in1, temp_in2, temp_out, conv=0;
	sc_lv<2*XORNBIT> temp_32;
	
	while(true){
		wait();
		temp_in1 = XOR_input1->read();
		temp_in2 = XOR_input2->read();
		
		for(unsigned j=0; j<XORNBIT; j++){
			temp_out[j] = temp_in1[j] ^ temp_in2[j];
		}
		temp_32 = (conv,temp_out);
		XOR_output->write(temp_32);	
	}
}
