// 16bit AND logic gate
#include <systemc.h>
#include "AND_gate.hpp"

using namespace std;

void AND_gate::andgate_operate(){
	
	sc_lv<ANDBIT> temp_in1, temp_in2, temp_out, conv=0;
	sc_lv<2*ANDBIT> temp_32;

	while(true){
		wait();
		temp_in1 = AND_input1->read();
		temp_in2 = AND_input2->read();
		
		for(unsigned j=0; j<ANDBIT; j++){
			temp_out[j] = temp_in1[j] & temp_in2[j];
		}
		
		temp_32 = (conv,temp_out);
		AND_output->write(temp_32);	
	}
}
