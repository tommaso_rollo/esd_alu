// 16bit Comparator
#include <systemc.h>
#include <math.h>
#include "Comp.hpp"

using namespace std;

void Comp::Comp_operate(){
	
	sc_uint<16> temp_out, conv=0;
	sc_uint<32> temp_32;

	while(true){
		wait();
		
		if(Comp_input1->read() > Comp_input2->read()) // input1 greater than input2
		{ temp_out = 1; }
		else if(Comp_input1->read() == Comp_input2->read()) // input1 = input2
		{ temp_out = 2; }
		else  
		{ temp_out = 4; }
	
	temp_32 = (conv, temp_out);
	Comp_output->write(temp_32);
	}
}
