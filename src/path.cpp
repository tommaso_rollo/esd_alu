//////////////////////////////////////
// component for M. Simonetti's ALU://
// 16bit input path, 32bit output   //
//////////////////////////////////////

#include <systemc.h>
#include "path.hpp"

using namespace std;

SC_HAS_PROCESS(path);

path::path(sc_module_name name): 
	sc_module(name), 
	alu("alu"),reg_in1("reg_in1"),reg_in2("reg_in2"),reg_out("reg_out") {

	// mapping
	reg_in1.reg_in(in_path1);
	reg_in1.load(load_in1);
	reg_in1.clk(path_clk);
	reg_in1.reg_out(out_reg1);

	reg_in2.reg_in(in_path2);
	reg_in2.load(load_in2);
	reg_in2.clk(path_clk);
	reg_in2.reg_out(out_reg2);

	alu.alu_in1(out_reg1);
	alu.alu_in2(out_reg2);
	alu.alu_sel_add1(path_add1);
	alu.alu_sel_add2(path_add2);
	alu.alu_shift(path_shift);
	alu.alu_operation(path_operation);
	alu.alu_out(out_alu);

	reg_out.reg32_in(out_alu);
	reg_out.load(load_out);
	reg_out.clk(path_clk);
	reg_out.reg32_out(out_path);
}
