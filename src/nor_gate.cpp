// from M. Simonetti repo
#include <systemc.h>
#include "nor_gate.hpp"

using namespace std;

void nor_gate::nor_gate_perform() {
	
	sc_lv<16> nor_gate_in1_temp, nor_gate_in2_temp, conv=0;	
	sc_lv<16> nor_gate_result;
	sc_lv<32> temp_32;

	while(true) {
		
		wait();
		nor_gate_in1_temp = nor_gate_in1->read();
		nor_gate_in2_temp = nor_gate_in2->read();			
		for (unsigned i=0; i<16; i++) {
        		nor_gate_result[i] = !(nor_gate_in1_temp[i] | nor_gate_in2_temp[i]);		
		}
	temp_32 = (conv,nor_gate_result);
	nor_gate_out->write(temp_32);
		
 	}   

}
   
