// register 32bit

#include <systemc.h>
#include "REG32.hpp"

using namespace std;

void REG32::reg32_operate(){

	sc_uint<REGBIT2> value=0;

	if (load) {	// enable update
            value = reg32_in->read();
        }
        
	reg32_out->write(value);
}
