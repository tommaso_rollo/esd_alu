// register

#include <systemc.h>
#include "REG.hpp"

using namespace std;

void REG::reg_operate(){

	sc_uint<REGBIT> value=0;

	if (load) {	// enable update
            value = reg_in->read();
        }
        
	reg_out->write(value);
}
