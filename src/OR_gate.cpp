// 16bit OR logic gate
#include <systemc.h>
#include "OR_gate.hpp"

using namespace std;

void OR_gate::orgate_operate(){
	
	sc_lv<ORNBIT> temp_in1, temp_in2, temp_out,conv=0;
	sc_lv<2*ORNBIT> temp_32;
	
	while(true){
		wait();
		temp_in1 = OR_input1->read();
		temp_in2 = OR_input2->read();
		
		for(unsigned j=0; j<ORNBIT; j++){
			temp_out[j] = temp_in1[j] | temp_in2[j];
		}
		temp_32=(conv,temp_out);
		OR_output->write(temp_32);	
	}
}
