// 32bit 8input Multiplexer
#include <systemc.h>
#include "MUX16.hpp"

using namespace std;

void MUX16::MUX16_operate(){
		
	sc_uint<MNBIT> m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15,m16;
	sc_uint<MUXSELBIT> sel;
	sc_uint<MNBIT> output;

	while(true){
		wait();
		sel = MUX16_sel->read();

		m1 = MUX16_in1->read();
		m2 = MUX16_in2->read();
		m3 = MUX16_in3->read();
		m4 = MUX16_in4->read();
		m5 = MUX16_in5->read();
		m6 = MUX16_in6->read();
		m7 = MUX16_in7->read();
		m8 = MUX16_in8->read();
		m9 = MUX16_in9->read();
		m10 = MUX16_in10->read();
		m11 = MUX16_in11->read();
		m12 = MUX16_in12->read();
		m13 = MUX16_in13->read();
		m14 = MUX16_in14->read();
		m15 = MUX16_in15->read();
		m16 = MUX16_in16->read();
		
		switch(sel){
			case 0 : output = m1;	break;
			case 1 : output = m2;	break;
			case 2 : output = m3;	break;
			case 3 : output = m4;	break;
			case 4 : output = m5;	break;
			case 5 : output = m6;	break;
			case 6 : output = m7;	break;
			case 7 : output = m8;	break;
			case 8 : output = m9;	break;
			case 9 : output = m10;	break;
			case 10 : output = m11;	break;
			case 11 : output = m12;	break;
			case 12 : output = m13;	break;
			case 13 : output = m14;	break;
			case 14 : output = m15;	break;
			case 15 : output = m16;	break;
		}

	MUX16_output->write(output);
	}
}
