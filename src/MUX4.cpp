// 16bit 4 input Multiplexer
#include <systemc.h>
#include "MUX4.hpp"

using namespace std;

void MUX4::MUX4_operate(){
		
	sc_uint<NBIT> m1,m2,m3,m4;
	sc_uint<SELBIT> sel;
	sc_uint<NBIT> output;

	while(true){
		wait();
		sel = MUX4_sel->read();

		m1 = MUX4_in1->read();
		m2 = MUX4_in2->read();
		m3 = MUX4_in3->read();
		m4 = MUX4_in4->read();
		
		switch(sel){
			case 0 : output = m1;	break;
			case 1 : output = m2;	break;
			case 2 : output = m3;	break;
			case 3 : output = m4;	break;		
		}

	MUX4_output->write(output);
	}
}
