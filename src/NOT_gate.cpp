// NOT logic gate
#include <systemc.h>
#include "NOT_gate.hpp"

using namespace std;

void NOT_gate::notgate_operate(){
	
	sc_lv<NOTNBIT> temp_in, temp_out;
	sc_lv<2*NOTNBIT> temp_32;
	sc_lv<NOTNBIT> conv=0;	// to convert output on 32bit

	while(true){
		wait();
		temp_in = NOT_input->read();
		for(unsigned j=0; j<NOTNBIT; j++){
			temp_out[j] = ~temp_in[j];}

		temp_32=(conv,temp_out);
		NOT_output->write(temp_32);	
	}
}
