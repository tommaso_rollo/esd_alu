// NOT logic gate
#include <systemc.h>
#include "clk.hpp"

using namespace std;

void clk::clk_operate(){

	while(true){
		clk_output.write(0);
      		wait(SEMI, SC_NS);
      		clk_output.write(1);
      		wait(SEMI, SC_NS);
	}
}
