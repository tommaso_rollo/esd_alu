// from M. Simonetti repo
#include <systemc.h>
#include "xnor_gate.hpp"

using namespace std;

void xnor_gate::xnor_gate_perform() {
	
	sc_lv<16> xnor_gate_in1_temp, xnor_gate_in2_temp,conv=0;	
	sc_lv<16> xnor_gate_result;
	sc_lv<32> temp_32;

	while(true) {
		
		wait();
		xnor_gate_in1_temp = xnor_gate_in1->read();
		xnor_gate_in2_temp = xnor_gate_in2->read();			
		for (unsigned i=0; i<16; i++) {
        		xnor_gate_result[i] = !(xnor_gate_in1_temp[i] ^ xnor_gate_in2_temp[i]);		
		}
	temp_32 = (conv,xnor_gate_result);
	xnor_gate_out->write(temp_32);
		
 	}   

}
   
