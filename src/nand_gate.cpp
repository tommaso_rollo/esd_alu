// from M. Simonetti Repo
#include <systemc.h>
#include "nand_gate.hpp"

using namespace std;

void nand_gate::nand_gate_perform() {
	
	sc_lv<16> nand_gate_in1_temp, nand_gate_in2_temp, conv=0;	
	sc_lv<16> nand_gate_result;
	sc_lv<32> temp_32;

	while(true) {
		
		wait();
		nand_gate_in1_temp = nand_gate_in1->read();
		nand_gate_in2_temp = nand_gate_in2->read();			
		for (unsigned i=0; i<16; i++) {
        		nand_gate_result[i] = !(nand_gate_in1_temp[i] & nand_gate_in2_temp[i]);		
		}
	temp_32=(conv,nand_gate_result);
	nand_gate_out->write(temp_32);
		
 	}   

}
   
