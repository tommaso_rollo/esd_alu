#include <systemc.h>
#include "alu_reg.hpp"

using namespace std;

SC_HAS_PROCESS(alu_reg);

alu_reg::alu_reg(sc_module_name name): 
	sc_module(name), 
        alu_1("alu_1"), reg16_1("reg16_1"), reg16_2("reg16_2"), reg32_1("reg32_1") {


  // Port Map
  reg16_1.reg_in(alu_reg_in1);
  reg16_1.clk(alu_reg_clk);
  reg16_1.load(alu_reg_load1);
  reg16_1.reg_out(reg16_temp1);

  reg16_2.reg_in(alu_reg_in2);
  reg16_2.clk(alu_reg_clk);
  reg16_2.load(alu_reg_load2);
  reg16_2.reg_out(reg16_temp2);
  
  alu_1.alu_in1(reg16_temp1);
  alu_1.alu_in2(reg16_temp2);
  alu_1.alu_sel(alu_reg_sel);
  alu_1.alu_op1(alu_reg_op1);
  alu_1.alu_op2(alu_reg_op2);
  alu_1.alu_line(alu_reg_line);
  alu_1.alu_LR(alu_reg_LR);
  alu_1.alu_out(reg32_temp);

  reg32_1.reg32_in(reg32_temp);
  reg32_1.clk(alu_reg_clk);
  reg32_1.load(alu_reg_load3);
  reg32_1.reg32_out(alu_reg_out);



}


