// 16bit 8input Multiplexer
#include <systemc.h>
#include "MUX8.hpp"

using namespace std;

void MUX8::MUX8_operate(){
		
	sc_uint<NBIT> m1,m2,m3,m4,m5,m6,m7,m8;
	sc_uint<SELBIT> sel;
	sc_uint<NBIT> output;

	while(true){
		wait();
		sel = MUX8_sel->read();

		m1 = MUX8_in1->read();
		m2 = MUX8_in2->read();
		m3 = MUX8_in3->read();
		m4 = MUX8_in4->read();
		m5 = MUX8_in5->read();
		m6 = MUX8_in6->read();
		m7 = MUX8_in7->read();
		m8 = MUX8_in8->read();
		
		switch(sel){
			case 0 : output = m1;	break;
			case 1 : output = m2;	break;
			case 2 : output = m3;	break;
			case 3 : output = m4;	break;
			case 4 : output = m5;	break;
			case 5 : output = m6;	break;
			case 6 : output = m7;	break;
			case 7 : output = m8;	break;
		}

	MUX8_output->write(output);
	}
}
