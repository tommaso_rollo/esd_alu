// Shifter
#include <systemc.h>
#include "SR.hpp"

using namespace std;

void SR::SR_operate(){
	
	sc_uint<SRNBIT> temp_in, conv=0;  // conv is for 32bit out conversion
	sc_uint<2*SRNBIT> temp_32;
	
	while(true){
		wait();
		temp_in = SR_input->read();
		
		if(sel->read() == SC_LOGIC_0)	//left shift: x2
		{	temp_in= temp_in << 1;}//quantity->read();}
		else if(sel->read() == SC_LOGIC_1)
		{	temp_in= temp_in >> 1;}//quantity->read();}
		
		temp_32 = (conv,temp_in);

	SR_output->write(temp_32);
	}
}
