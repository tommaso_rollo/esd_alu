#include <systemc.h>
#include "alu.hpp"

using namespace std;

SC_HAS_PROCESS(alu);

alu::alu(sc_module_name name): 
	sc_module(name), 
	adder1("adder1"),multi1("multi1"),sr1("sr1"),comp1("comp1"),not1("not1"),and1("and1"),or1("or1"),xor1("xor1"),nand1("nand1"),nor1("nor1"),xnor1("xnor1"),mux_sel1("mux_sel1"),mux_sel2("mux_sel2"),muxout("muxout") {
	
	zeros = 0;

	// gates mapping
	adder1.add_incr_in1(alu_in1);
	adder1.add_incr_in2(alu_in2);
	adder1.add_incr_op1(alu_op1);
	adder1.add_incr_op2(alu_op2);
	adder1.add_incr_out(outadd);
//	adder1.add_incr_of(alu_of);

	multi1.Multi_input1(alu_in1);
	multi1.Multi_input2(alu_in2);
	multi1.Multi_output(outmult);

	sr1.SR_input(mux2_shf);
//	sr1.quantity(1);	// 1bit shift
	sr1.sel(alu_LR);
	sr1.SR_output(outshf);

	comp1.Comp_input1(alu_in1);
	comp1.Comp_input2(alu_in2);
	comp1.Comp_output(outcomp);

	not1.NOT_input(mux2_not);
	not1.NOT_output(outnot);

	and1.AND_input1(alu_in1);
	and1.AND_input2(alu_in2);
	and1.AND_output(outand);

	or1.OR_input1(alu_in1);
	or1.OR_input2(alu_in2);
	or1.OR_output(outor);

	xor1.XOR_input1(alu_in1);
	xor1.XOR_input2(alu_in2);
	xor1.XOR_output(outxor);

	nand1.nand_gate_in1(alu_in1);
	nand1.nand_gate_in2(alu_in2);
	nand1.nand_gate_out(outnand);

	nor1.nor_gate_in1(alu_in1);
	nor1.nor_gate_in2(alu_in2);
	nor1.nor_gate_out(outnor);

	xnor1.xnor_gate_in1(alu_in1);
	xnor1.xnor_gate_in2(alu_in2);
	xnor1.xnor_gate_out(outxnor);
	
	// mux mapping
	mux_sel1.mux2_in1(alu_in1);
	mux_sel1.mux2_in2(alu_in2);
	mux_sel1.mux2_sel(alu_line);
	mux_sel1.mux2_output(mux2_not);

	mux_sel2.mux2_in1(alu_in1);
	mux_sel2.mux2_in2(alu_in2);
	mux_sel2.mux2_sel(alu_line);
	mux_sel2.mux2_output(mux2_shf);

	muxout.MUX16_in1(outnot);
	muxout.MUX16_in2(outshf);
	muxout.MUX16_in3(outand);
	muxout.MUX16_in4(outor);
	muxout.MUX16_in5(outxor);
	muxout.MUX16_in6(outnor);
	muxout.MUX16_in7(outnand);
	muxout.MUX16_in8(outxnor);
	muxout.MUX16_in9(outcomp);
	muxout.MUX16_in10(outmult);
	muxout.MUX16_in11(outadd);
	muxout.MUX16_in12(zeros);	
	muxout.MUX16_in13(zeros);
	muxout.MUX16_in14(zeros);
	muxout.MUX16_in15(zeros);
	muxout.MUX16_in16(zeros);
	muxout.MUX16_sel(alu_sel);
	muxout.MUX16_output(alu_out);
}
	
