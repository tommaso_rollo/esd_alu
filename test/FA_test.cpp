#include <systemc.h>
#include <string>
#include <math.h>
#include "FA.hpp"

using namespace std;

SC_MODULE(TB){
   public:	
	sc_signal<sc_uint<ANBIT> >	input1;
	sc_signal<sc_uint<ANBIT> >	input2;
	sc_signal<sc_uint<2*ANBIT> > 	output;
//	sc_signal<sc_logic>		o_f;	// overflow
	
	FA tb_FA;
	
	SC_CTOR(TB):tb_FA("tb_FA"){
		SC_THREAD(testing);
		tb_FA.FA_input1(this -> input1);
		tb_FA.FA_input2(this -> input2);
		tb_FA.FA_output(this -> output);
//		tb_FA.FA_over(this -> o_f);
		set_vtest();	}
   
   private:
	static const unsigned TSIZE = 2;
	unsigned vtest_in1[TSIZE],vtest_in2[TSIZE], vtest_out[TSIZE]; 
//	sc_logic vtest_of[TSIZE];

	void testing(){
		for (unsigned j=0;j<TSIZE;j++)
		{
			// input test
			cout << endl << " Test " << j+1 << " of " << TSIZE<<endl;		
			input1.write(vtest_in1[j]);
			cout << "FA first input: " << vtest_in1[j] <<endl;
			input2.write(vtest_in2[j]);
			cout << "FA second input: " << vtest_in2[j] <<endl<<endl;
	
			wait(2,SC_NS);	// 2 ns
			
//			vtest_of[j] = o_f.read();
//			cout << "FA Overflow: "<<vtest_of[j] <<endl;
			vtest_out[j] = output.read();
			cout << "FA output: "<<vtest_out[j] <<endl << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<endl;
		}
	}
	
	// set test values	
	void set_vtest(){
		vtest_in1[0] = 15;
		vtest_in2[0] = 8;
		
		vtest_in1[1] = pow(2,ANBIT)-1;	// 2^ANBIT-1
		vtest_in2[1] = pow(2,ANBIT)-1; }
};

int sc_main(int argc, char* argv[])
{
  TB test("FA test");

  cout << endl << "%%%%  STARTED  %%%%" << endl;

  sc_start();

  cout << endl << "%%%%  FINISHED  %%%%" << endl;  

  return 0;
}
