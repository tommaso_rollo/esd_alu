#include <systemc.h>
#include <string>
#include "alu.hpp"

using namespace std;

SC_MODULE(TB){
   public:	
	sc_signal<sc_uint<ALUNBIT> >	input1,input2;
	sc_signal<sc_uint<ALUSELBIT> >	sel;
	sc_signal<sc_uint<2*ALUNBIT> > output1;
	sc_signal<bool> op1,op2,line,LR;
	
	alu	tb_alu;
	
	SC_CTOR(TB):tb_alu("tb_alu"){
		SC_THREAD(testing);
		tb_alu.alu_in1(this -> input1);
		tb_alu.alu_in2(this -> input2);
		tb_alu.alu_sel(this -> sel);
		tb_alu.alu_line(this -> line);
		tb_alu.alu_LR(this -> LR);
		tb_alu.alu_op1(this -> op1);
		tb_alu.alu_op2(this -> op2);

		tb_alu.alu_out(this -> output1);
		set_vtest();	}
   
   private:
	static const unsigned TSIZE = 2;
	unsigned vtest_in1[TSIZE],vtest_in2[TSIZE],vtest_sel[TSIZE], vtest_out[TSIZE];
	bool vtest_op1[TSIZE],vtest_op2[TSIZE],vtest_line[TSIZE],vtest_LR[TSIZE];

	void testing(){
		for (unsigned j=0;j<TSIZE;j++)
		{
			// input test
			cout << endl << " Test " << j+1 << " of " << TSIZE<<endl;
			input1.write(vtest_in1[j]);
			cout << "Functions (sel-input list): " << endl;
			cout << "	0: NOT" << endl;
			cout << "	1: Shifter" << endl;
			cout << "	2: AND" << endl;
			cout << "	3: OR" << endl;
			cout << "	4: XOR" << endl;
			cout << "	5: NOR" << endl;
			cout << "	6: NAND" << endl;
			cout << "	7: XNOR" << endl;
			cout << "	8: Comparator" << endl;
			cout << "	9: Multiplier" << endl;
			cout << "	10: Adder/Incrementer" << endl<<endl;
			cout << "Alu input1: " << vtest_in1[j] <<endl;
			input2.write(vtest_in2[j]);
			cout << "Alu input2: " << vtest_in2[j] <<endl;
			sel.write(vtest_sel[j]);
			cout << "Alu function: " << vtest_sel[j] <<endl;
			op1.write(vtest_op1[j]);
			cout << "Adder op1: " << vtest_op1[j] <<endl;
			op2.write(vtest_op2[j]);
			cout << "Adder op2: " << vtest_op2[j] <<endl;
			line.write(vtest_line[j]);
			cout << "Single gates line (0: first, 1: second): " << vtest_line[j] <<endl;
			LR.write(vtest_LR[j]);
			cout << "Shifter direction (0: left, 1: right): " << vtest_LR[j] <<endl<<endl;

			wait(2,SC_NS);	// 2 ns

			vtest_out[j] = output1.read();
			cout << "Alu output: "<<vtest_out[j] <<endl << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<endl;
		}
	}
	
	// set test values	
	void set_vtest(){
		vtest_in1[0] = 1;
		vtest_in2[0] = 5;
		vtest_sel[0] = 9;
		vtest_op1[0] = 1;
		vtest_op2[0] = 1;
		vtest_line[0] = 0;
		vtest_LR[0] = 1;

		vtest_in1[1] = 8;
		vtest_in2[1] = 32;
		vtest_sel[1] = 1;
		vtest_op1[1] = 1;
		vtest_op2[1] = 1;
		vtest_line[1] = 1;
		vtest_LR[1] = 0; }
};

int sc_main(int argc, char* argv[])
{
  TB test("test");

  cout << endl << "%%%%  STARTED  %%%%" << endl;

  sc_start();

  cout << endl << "%%%%  FINISHED  %%%%" << endl;  

  return 0;
}
