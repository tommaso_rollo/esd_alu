#include <systemc.h>
#include <string>
#include <math.h>
#include "MUX8.hpp"

using namespace std;

SC_MODULE(TB){
   public:	
	sc_signal<sc_uint<NBIT> >	op1, op2, op3, op4, op5, op6, op7, op8;
	sc_signal<sc_uint<SELBIT> >	select;
	sc_signal<sc_uint<NBIT> > 	output;
	
	MUX8 tb_MUX8;
	
	SC_CTOR(TB):tb_MUX8("tb_MUX8"){
		SC_THREAD(testing);
		tb_MUX8.MUX8_in1(this -> op1);
		tb_MUX8.MUX8_in2(this -> op2);
		tb_MUX8.MUX8_in3(this -> op3);
		tb_MUX8.MUX8_in4(this -> op4);
		tb_MUX8.MUX8_in5(this -> op5);
		tb_MUX8.MUX8_in6(this -> op6);
		tb_MUX8.MUX8_in7(this -> op7);
		tb_MUX8.MUX8_in8(this -> op8);
		tb_MUX8.MUX8_sel(this -> select);
		tb_MUX8.MUX8_output(this -> output);
		set_vtest();	}
   
   private:

	static const unsigned TSIZE = 2;
	unsigned vtest_in1[TSIZE],vtest_in2[TSIZE], vtest_in3[TSIZE],vtest_in4[TSIZE],vtest_in5[TSIZE],vtest_in6[TSIZE], vtest_in7[TSIZE],vtest_in8[TSIZE];
	unsigned vtest_out[TSIZE]; 
	unsigned vtest_sel[TSIZE];

	void testing(){
		for (unsigned j=0;j<TSIZE;j++)
		{
			// input test
			cout << endl << " Test " << j+1 << " of " << TSIZE<<endl;		
			op1.write(vtest_in1[j]);
			op2.write(vtest_in2[j]);
			op3.write(vtest_in3[j]);
			op4.write(vtest_in4[j]);
			op5.write(vtest_in5[j]);
			op6.write(vtest_in6[j]);
			op7.write(vtest_in7[j]);
			op8.write(vtest_in8[j]);
			cout << "MUX8 inputs: " << vtest_in1[j] <<endl<<vtest_in2[j]<<endl<<vtest_in3[j]<<endl<<vtest_in4[j]<<endl<<vtest_in5[j]<<endl<<vtest_in6[j]<<endl<<vtest_in7[j]<<endl<<vtest_in8[j]<<endl;
			
			select.write(vtest_sel[j]);
			cout << "MUX8 select input: " << vtest_sel[j] <<endl<<endl;
	
			wait(2,SC_NS);	// 2 ns

			vtest_out[j] = output.read();
			cout << "MUX8 output: "<<vtest_out[j] <<endl << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<endl;
		}
	}
	
	// set test values	
	void set_vtest(){
		vtest_in1[0] = 15;
		vtest_in2[0] = 8;
		vtest_in3[0] = 215;
		vtest_in4[0] = 3;
		vtest_in5[0] = 175;
		vtest_in6[0] = 864;
		vtest_in7[0] = 1500;
		vtest_in8[0] = 0;
		vtest_sel[0] = 4;
		
		vtest_in1[1] = pow(2,NBIT)-1;	// 2^NBIT-1
		vtest_in2[1] = pow(2,NBIT)-1; 
		vtest_in3[1] = 2177;
		vtest_in4[1] = 0;
		vtest_in5[1] = 0;
		vtest_in6[1] = 7;
		vtest_in7[1] = 4444;
		vtest_in8[1] = 998;
		vtest_sel[1] = 1;	}
};

int sc_main(int argc, char* argv[])
{
  TB test("MUX8 test");

  cout << endl << "%%%%  STARTED  %%%%" << endl;

  sc_start();

  cout << endl << "%%%%  FINISHED  %%%%" << endl;  

  return 0;
}
