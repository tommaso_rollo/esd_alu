#include <systemc.h>
#include <string>
#include <math.h>
#include "MUX4.hpp"

using namespace std;

SC_MODULE(TB){
   public:	
	sc_signal<sc_uint<NBIT> >	op1, op2, op3, op4;
	sc_signal<sc_uint<SELBIT> >	select;
	sc_signal<sc_uint<NBIT> > 	output;
	
	MUX4 tb_MUX4;
	
	SC_CTOR(TB):tb_MUX4("tb_MUX4"){
		SC_THREAD(testing);
		tb_MUX4.MUX4_in1(this -> op1);
		tb_MUX4.MUX4_in2(this -> op2);
		tb_MUX4.MUX4_in3(this -> op3);
		tb_MUX4.MUX4_in4(this -> op4);
		tb_MUX4.MUX4_sel(this -> select);
		tb_MUX4.MUX4_output(this -> output);
		set_vtest();	}
   
   private:

	static const unsigned TSIZE = 2;
	unsigned vtest_in1[TSIZE],vtest_in2[TSIZE], vtest_in3[TSIZE],vtest_in4[TSIZE];
	unsigned vtest_out[TSIZE]; 
	unsigned vtest_sel[TSIZE];

	void testing(){
		for (unsigned j=0;j<TSIZE;j++)
		{
			// input test
			cout << endl << " Test " << j+1 << " of " << TSIZE<<endl;		
			op1.write(vtest_in1[j]);
			op2.write(vtest_in2[j]);
			op3.write(vtest_in3[j]);
			op4.write(vtest_in4[j]);
			cout << "MUX4 inputs: " <<endl<< vtest_in1[j] <<endl<<vtest_in2[j]<<endl<<vtest_in3[j]<<endl<<vtest_in4[j]<<endl;
			
			select.write(vtest_sel[j]);
			cout << "MUX4 select input: " << vtest_sel[j] <<endl<<endl;
	
			wait(2,SC_NS);	// 2 ns

			vtest_out[j] = output.read();
			cout << "MUX4 output: "<<vtest_out[j] <<endl << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<endl;
		}
	}
	
	// set test values	
	void set_vtest(){
		vtest_in1[0] = 15;
		vtest_in2[0] = 8;
		vtest_in3[0] = 25;
		vtest_in4[0] = 3;
		vtest_sel[0] = 1;
		
		vtest_in1[1] = pow(2,NBIT)-1;	// 2^NBIT-1
		vtest_in2[1] = pow(2,NBIT)-1; 
		vtest_in3[1] = 2187;
		vtest_in4[1] = 0;
		vtest_sel[1] = 3;
		}
};

int sc_main(int argc, char* argv[])
{
  TB test("MUX4 test");

  cout << endl << "%%%%  STARTED  %%%%" << endl;

  sc_start();

  cout << endl << "%%%%  FINISHED  %%%%" << endl;  

  return 0;
}
