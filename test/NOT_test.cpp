#include <systemc.h>
#include <string>
#include "NOT_gate.hpp"

using namespace std;

SC_MODULE(TB){
   public:	
	sc_signal<sc_uint<NOTNBIT> >	input1;
	sc_signal<sc_uint<2*NOTNBIT> > output1;
	
	NOT_gate	tb_NOT_gate;
	
	SC_CTOR(TB):tb_NOT_gate("tb_NOT_gate"){
		SC_THREAD(testing);
		tb_NOT_gate.NOT_input(this -> input1);
		tb_NOT_gate.NOT_output(this -> output1);
		set_vtest();	}
   
   private:
	static const unsigned TSIZE = 2;
	unsigned vtest_in[TSIZE], vtest_out[TSIZE];

	void testing(){
		for (unsigned j=0;j<TSIZE;j++)
		{
			// input test
			cout << endl << " Test " << j+1 << " of " << TSIZE<<endl;
			input1.write(vtest_in[j]);
			cout << "NOT_gate input: " << vtest_in[j] <<endl<<endl;

			wait(2,SC_NS);	// 2 ns

			vtest_out[j] = output1.read();
			cout << "NOT_gate output: "<<vtest_out[j] <<endl << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<endl;
		}
	}
	
	// set test values	
	void set_vtest(){
		vtest_in[0] = 1;
		vtest_in[1] = 0; }
};

int sc_main(int argc, char* argv[])
{
  TB test("test");

  cout << endl << "%%%%  STARTED  %%%%" << endl;

  sc_start();

  cout << endl << "%%%%  FINISHED  %%%%" << endl;  

  return 0;
}
