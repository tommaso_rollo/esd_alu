#include <systemc.h>
#include <string>
#include <math.h>
#include "MUX16.hpp"

using namespace std;

SC_MODULE(TB){
   public:	
	sc_signal<sc_uint<MNBIT> >	op1, op2, op3, op4, op5, op6, op7, op8,op9, op10, op11, op12, op13, op14, op15, op16;
	sc_signal<sc_uint<MUXSELBIT> >	select;
	sc_signal<sc_uint<MNBIT> > 	output;
	
	MUX16 tb_MUX16;
	
	SC_CTOR(TB):tb_MUX16("tb_MUX16"){
		SC_THREAD(testing);
		tb_MUX16.MUX16_in1(this -> op1);
		tb_MUX16.MUX16_in2(this -> op2);
		tb_MUX16.MUX16_in3(this -> op3);
		tb_MUX16.MUX16_in4(this -> op4);
		tb_MUX16.MUX16_in5(this -> op5);
		tb_MUX16.MUX16_in6(this -> op6);
		tb_MUX16.MUX16_in7(this -> op7);
		tb_MUX16.MUX16_in8(this -> op8);
		tb_MUX16.MUX16_in9(this -> op9);
		tb_MUX16.MUX16_in10(this -> op10);
		tb_MUX16.MUX16_in11(this -> op11);
		tb_MUX16.MUX16_in12(this -> op12);
		tb_MUX16.MUX16_in13(this -> op13);
		tb_MUX16.MUX16_in14(this -> op14);
		tb_MUX16.MUX16_in15(this -> op15);
		tb_MUX16.MUX16_in16(this -> op16);
		tb_MUX16.MUX16_sel(this -> select);
		tb_MUX16.MUX16_output(this -> output);
		set_vtest();	}
   
   private:

	static const unsigned TSIZE = 2;
	unsigned vtest_in1[TSIZE],vtest_in2[TSIZE], vtest_in3[TSIZE],vtest_in4[TSIZE],vtest_in5[TSIZE],vtest_in6[TSIZE], vtest_in7[TSIZE],vtest_in8[TSIZE],vtest_in9[TSIZE],vtest_in10[TSIZE], vtest_in11[TSIZE],vtest_in12[TSIZE],vtest_in13[TSIZE],vtest_in14[TSIZE], vtest_in15[TSIZE],vtest_in16[TSIZE];
	unsigned vtest_out[TSIZE]; 
	unsigned vtest_sel[TSIZE];

	void testing(){
		for (unsigned j=0;j<TSIZE;j++)
		{
			// input test
			cout << endl << " Test " << j+1 << " of " << TSIZE<<endl;		
			op1.write(vtest_in1[j]);
			op2.write(vtest_in2[j]);
			op3.write(vtest_in3[j]);
			op4.write(vtest_in4[j]);
			op5.write(vtest_in5[j]);
			op6.write(vtest_in6[j]);
			op7.write(vtest_in7[j]);
			op8.write(vtest_in8[j]);
			op9.write(vtest_in9[j]);
			op10.write(vtest_in10[j]);
			op11.write(vtest_in11[j]);
			op12.write(vtest_in12[j]);
			op13.write(vtest_in13[j]);
			op14.write(vtest_in14[j]);
			op15.write(vtest_in15[j]);
			op16.write(vtest_in16[j]);
			cout << "MUX16 inputs: " << vtest_in1[j] <<endl<<vtest_in2[j]<<endl<<vtest_in3[j]<<endl<<vtest_in4[j]<<endl<<vtest_in5[j]<<endl<<vtest_in6[j]<<endl<<vtest_in7[j]<<endl<<vtest_in8[j]<<endl<< vtest_in9[j] <<endl<<vtest_in10[j]<<endl<<vtest_in11[j]<<endl<<vtest_in12[j]<<endl<<vtest_in13[j]<<endl<<vtest_in14[j]<<endl<<vtest_in15[j]<<endl<<vtest_in16[j]<<endl;
			
			select.write(vtest_sel[j]);
			cout << "MUX16 select input: " << vtest_sel[j] <<endl<<endl;
	
			wait(2,SC_NS);	// 2 ns

			vtest_out[j] = output.read();
			cout << "MUX16 output: "<<vtest_out[j] <<endl << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<endl;
		}
	}
	
	// set test values	
	void set_vtest(){
		vtest_in1[0] = 15;
		vtest_in2[0] = 8;
		vtest_in3[0] = 215;
		vtest_in4[0] = 3;
		vtest_in5[0] = 175;
		vtest_in6[0] = 864;
		vtest_in7[0] = 1500;
		vtest_in8[0] = 0;
		vtest_in9[0] = 15;
		vtest_in10[0] = 8;
		vtest_in11[0] = 215;
		vtest_in12[0] = 3;
		vtest_in13[0] = 175;
		vtest_in14[0] = 864;
		vtest_in15[0] = 1500;
		vtest_in16[0] = 0;
		vtest_sel[0] = 4;

		vtest_in1[1] = pow(2,MNBIT)-1;	// 2^MNBIT-1
		vtest_in2[1] = pow(2,MNBIT)-1; 
		vtest_in3[1] = 2177;
		vtest_in4[1] = 0;
		vtest_in5[1] = 0;
		vtest_in6[1] = 7;
		vtest_in7[1] = 4444;
		vtest_in8[1] = 998;
		vtest_in9[1] = 15;
		vtest_in10[1] = 8;
		vtest_in11[1] = 215;
		vtest_in12[1] = 3;
		vtest_in13[1] = 175;
		vtest_in14[1] = 864;
		vtest_in15[1] = 1500;
		vtest_in16[1] = 0;
		vtest_sel[1] = 15;	}
};

int sc_main(int argc, char* argv[])
{
  TB test("MUX16 test");

  cout << endl << "%%%%  STARTED  %%%%" << endl;

  sc_start();

  cout << endl << "%%%%  FINISHED  %%%%" << endl;  

  return 0;
}
