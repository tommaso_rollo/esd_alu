#include <systemc.h>
#include <string>
#include "alu_reg.hpp"

using namespace std;

SC_MODULE(TB){
   public:	
	sc_signal<sc_uint<16> >	input1,input2;
	sc_signal<sc_uint<4> >	sel;
	sc_signal<sc_uint<32> > output1;
	sc_signal<bool> op1,op2,line,LR,tb_clk,tb_load1,tb_load2,tb_load3;
	
	alu_reg	tb_alu_reg;
	
	SC_CTOR(TB):tb_alu_reg("tb_alu_reg"){
                SC_THREAD(clock_gen);
       		SC_THREAD(testing);
		tb_alu_reg.alu_reg_in1(this -> input1);
		tb_alu_reg.alu_reg_in2(this -> input2);
		tb_alu_reg.alu_reg_sel(this -> sel);
		tb_alu_reg.alu_reg_line(this -> line);
		tb_alu_reg.alu_reg_LR(this -> LR);
		tb_alu_reg.alu_reg_op1(this -> op1);
		tb_alu_reg.alu_reg_op2(this -> op2);
		tb_alu_reg.alu_reg_clk(this -> tb_clk);
		tb_alu_reg.alu_reg_load1(this -> tb_load1);
		tb_alu_reg.alu_reg_load2(this -> tb_load2);
		tb_alu_reg.alu_reg_load3(this -> tb_load3);

		tb_alu_reg.alu_reg_out(this -> output1);
		set_vtest();	}
   
   private:
	static const unsigned TSIZE = 2;
	unsigned vtest_in1[TSIZE],vtest_in2[TSIZE],vtest_sel[TSIZE], vtest_out[TSIZE];
	bool vtest_op1[TSIZE],vtest_op2[TSIZE],vtest_line[TSIZE],vtest_LR[TSIZE];
	bool load1_values[TSIZE], load2_values[TSIZE], load3_values[TSIZE];
	//unsigned clock_count;
    	unsigned clock_period;
    
	void clock_gen() {

     		while(true){
		tb_clk.write(0);
      		wait(0.5*clock_period, SC_NS); // duty del 50%
      		tb_clk.write(1);
      		wait(0.5*clock_period, SC_NS);
	}
   	}

	void testing(){
		for (unsigned j=0;j<TSIZE;j++)
		{

			// input test
			cout << endl << " Test " << j+1 << " of " << TSIZE<<endl;
			tb_load1.write(load1_values[j]);
			tb_load2.write(load2_values[j]);
			tb_load3.write(load3_values[j]);
	
			input1.write(vtest_in1[j]);
			cout << "Functions (sel-input list): " << endl;
			cout << "	0: NOT" << endl;
			cout << "	1: Shifter" << endl;
			cout << "	2: AND" << endl;
			cout << "	3: OR" << endl;
			cout << "	4: XOR" << endl;
			cout << "	5: NOR" << endl;
			cout << "	6: NAND" << endl;
			cout << "	7: XNOR" << endl;
			cout << "	8: Comparator" << endl;
			cout << "	9: Multiplier" << endl;
			cout << "	10: Adder/Incrementer" << endl<<endl;
			cout << "Alu input1: " << vtest_in1[j] <<endl;
			input2.write(vtest_in2[j]);
			cout << "Alu input2: " << vtest_in2[j] <<endl;

				
			sel.write(vtest_sel[j]);
			cout << "Alu function: " << vtest_sel[j] <<endl;
			op1.write(vtest_op1[j]);
			cout << "Adder op1: " << vtest_op1[j] <<endl;
			op2.write(vtest_op2[j]);
			cout << "Adder op2: " << vtest_op2[j] <<endl;
			line.write(vtest_line[j]);
			cout << "Single gates line (0: first, 1: second): " << vtest_line[j] <<endl;
			LR.write(vtest_LR[j]);
			cout << "Shifter direction (0: left, 1: right): " << vtest_LR[j] <<endl<<endl;

			wait(clock_period,SC_NS); 
           		
			vtest_out[j] = output1.read();
			cout << "Alu output: "<<vtest_out[j] <<endl << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<endl;
		}
	}
	
	// set test values	
	void set_vtest(){


 		//Inizializzazione del contatore dei cicli di clock
   		//clock_count=0;
    		//Tclk [ns]
       		clock_period=10;

		load1_values[0] = 1;
		load1_values[1] = 1;
		load2_values[0] = 1;
		load2_values[1] = 1;
		load3_values[0] = 1;
		load3_values[1] = 1;


		vtest_in1[0] = 12;
		vtest_in1[1] = 5;		
		
		vtest_in2[0] = 5;
		vtest_in2[1] = 5;
		
		vtest_sel[0] = 9;
		vtest_sel[1] = 2;
		
		vtest_op1[0] = 1;
		vtest_op1[1] = 1;
		
                vtest_op2[0] = 1;
		vtest_op2[1] = 1;
		
		vtest_line[0] = 0;
		vtest_line[1] = 0;
		
		vtest_LR[0] = 1;
		vtest_LR[1] = 1;
		}
};

int sc_main(int argc, char* argv[])
{
  TB test("test");

  cout << endl << "%%%%  STARTED  %%%%" << endl;
  
  int sim_time=1000;
  sc_set_time_resolution(1,SC_NS);
  sc_start(sim_time,SC_NS);
  
  cout << endl << "%%%%  FINISHED  %%%%" << endl;  

  return 0;
}
