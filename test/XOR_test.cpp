#include <systemc.h>
#include <string>
#include <math.h>
#include "XOR_gate.hpp"

using namespace std;

SC_MODULE(TB){
   public:	
	sc_signal<sc_uint<XORNBIT> >	input1;
	sc_signal<sc_uint<XORNBIT> >	input2;
	sc_signal<sc_uint<2*XORNBIT> > 	output;
	
	XOR_gate tb_XOR_gate;
	
	SC_CTOR(TB):tb_XOR_gate("tb_XOR_gate"){
		SC_THREAD(testing);
		tb_XOR_gate.XOR_input1(this -> input1);
		tb_XOR_gate.XOR_input2(this -> input2);
		tb_XOR_gate.XOR_output(this -> output);
		set_vtest();	}
   
   private:
	static const unsigned TSIZE = 2;
	unsigned vtest_in1[TSIZE],vtest_in2[TSIZE], vtest_out[TSIZE];

	void testing(){
		for (unsigned j=0;j<TSIZE;j++)
		{
			// input test
			cout << endl << " Test " << j+1 << " of " << TSIZE<<endl;
			input1.write(vtest_in1[j]);
			cout << "XOR_gate first input: " << vtest_in1[j] <<endl;
			input2.write(vtest_in2[j]);
			cout << "XOR_gate second input: " << vtest_in2[j] <<endl<<endl;

			wait(2,SC_NS);	// 2 ns

			vtest_out[j] = output.read();
			cout << "XOR_gate output: "<<vtest_out[j] <<endl << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<endl;
		}
	}
	
	// set test values	
	void set_vtest(){
		vtest_in1[0] = 1;
		vtest_in2[0] = 0;
		
		vtest_in1[1] = pow(2,XORNBIT)-1;	// 2^XORNBIT-1
		vtest_in2[1] = pow(2,XORNBIT)-1; }
};

int sc_main(int argc, char* argv[])
{
  TB test("XOR test");

  cout << endl << "%%%%  STARTED  %%%%" << endl;

  sc_start();

  cout << endl << "%%%%  FINISHED  %%%%" << endl;  

  return 0;
}
